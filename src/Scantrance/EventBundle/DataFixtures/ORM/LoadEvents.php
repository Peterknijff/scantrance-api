<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\EventBundle\Entity\Event;
use Scantrance\EventBundle\Entity\EventType;

class LoadEvents extends AbstractFixture implements OrderedFixtureInterface  {

    public function load(ObjectManager $manager)
    {
        $eventType = $manager->getRepository("EventBundle:EventType");
        $eventType = $eventType->findOneBy(array('name' => 'Festival'));

        $event = new Event();
        $event->setName('Festival X');
        $event->setEventType($eventType);
        $event->setDescription('Dit festival is echt de bom, oorspronkelijk ontstaan vanuit Project X nu Festival X!');
        $manager->persist($event);

        $event = new Event();
        $event->setName('Solar Festival');
        $event->setEventType($eventType);
        $event->setDescription('Voor alle creatievelingen en gekke boys !');
        $manager->persist($event);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }

}