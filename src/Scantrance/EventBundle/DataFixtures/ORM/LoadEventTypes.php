<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\EventBundle\Entity\EventType;

class LoadEventTypes extends AbstractFixture implements OrderedFixtureInterface{

    public function load(ObjectManager $manager)
    {
        $eventType = new EventType();
        $eventType->setName('Festival');
        $eventType->setDescription('Festival');
        $manager->persist($eventType);

        $eventType = new EventType();
        $eventType->setName('Beurs');
        $eventType->setDescription('Beurs');
        $manager->persist($eventType);

        $eventType = new EventType();
        $eventType->setName('Theater');
        $eventType->setDescription('Theater');
        $manager->persist($eventType);

        $eventType = new EventType();
        $eventType->setName('Klein evenement');
        $eventType->setDescription('Klein evenement');
        $manager->persist($eventType);

        $eventType = new EventType();
        $eventType->setName('Groot evenement');
        $eventType->setDescription('Groot evenement');
        $manager->persist($eventType);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}