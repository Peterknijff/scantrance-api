<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\EventBundle\Entity\EventSchedule;

class LoadEventSchedule extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager)
    {
        $event = $manager->getRepository("EventBundle:Event");
        $event = $event->findOneBy(array('name' => 'Festival X'));

        $eventSchedule = new EventSchedule();
        $eventSchedule->setEvent($event);
        $eventSchedule->setDateStart(new \DateTime('2015-05-01'));
        $eventSchedule->setDateEnd(new \DateTime('2015-06-01'));
        $eventSchedule->setTimeStart(new \DateTime('12:00:00'));
        $eventSchedule->setTimeEnd(new \DateTime('00:00:00'));

        $manager->persist($eventSchedule);

        $eventSchedule = new EventSchedule();
        $eventSchedule->setEvent($event);
        $eventSchedule->setDateStart(new \DateTime('2015-06-01'));
        $eventSchedule->setDateEnd(new \DateTime('2015-07-01'));
        $eventSchedule->setTimeStart(new \DateTime('12:00:00'));
        $eventSchedule->setTimeEnd(new \DateTime('00:00:00'));

        $manager->persist($eventSchedule);


        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }
}