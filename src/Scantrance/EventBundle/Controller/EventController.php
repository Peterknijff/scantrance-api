<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19-04-15
 * Time: 22:31
 */

namespace Scantrance\EventBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\View\View;


class EventController extends FOSRestController{
//
//    /**
//     * Return the overall event list.
//     *
//     * //@Secure(roles="ROLE_API")
//     * @ApiDoc(
//     *   resource = true,
//     *   description = "Return the overall Event List",
//     *   statusCodes = {
//     *     200 = "Returned when successful",
//     *     404 = "Returned when the user is not found"
//     *   }
//     * )
//     *
//     * @return View
//     */
    public function getEventsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('EventBundle:Event');
        $events = $events->findAll();

        if (!$events) {
            throw $this->createNotFoundException('Data not found.');
        }

        $view = View::create();
        $view->setData($events)->setStatusCode(200);

        return $view;
    }

}