<?php

namespace Scantrance\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Scantrance\AddressBundle\Entity\Company;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity
 */
class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Scantrance\EventBundle\Entity\EventType", inversedBy="event")
     *
     * @ORM\JoinColumn(name="event_type_id", referencedColumnName="id")
     *
     **/
    private $event_type;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set event_type
     *
     * @param EventType $event_type
     * @return Event
     */
    public function setEventType(EventType $event_type = null)
    {
        $this->event_type = $event_type;

        return $this;
    }

    /**
     * Get event_type
     *
     * @return EventType
     */
    public function getEventType()
    {
        return $this->event_type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
