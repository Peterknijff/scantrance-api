<?php

namespace Scantrance\WalletBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use Scantrance\UserBundle\Entity\Visitor;
use Scantrance\UserBundle\Entity\VisitorGroup;

/**
 * Wallet
 *
 * @ORM\Table(name="wallet")
 * @ORM\Entity
 */
class Wallet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Exclude
     * @ORM\OneToOne(targetEntity="Scantrance\UserBundle\Entity\Visitor", inversedBy="wallet")
     * @ORM\JoinColumn(name="visitor_id", referencedColumnName="id")
     **/
    private $visitor;

    /**
     * @Exclude
     * @ORM\OneToOne(targetEntity="Scantrance\UserBundle\Entity\VisitorGroup", inversedBy="wallet")
     * @ORM\JoinColumn(name="visitor_group_id", referencedColumnName="id")
     **/
    private $visitor_group;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="pincode", type="integer", length=4, nullable=false)
     */
    private $pincode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visitor
     *
     * @param Visitor $visitor
     * @return Visitor
     */
    public function setVisitor(Visitor $visitor)
    {
        $this->visitor = $visitor;

        return $this;
    }

    /**
     * Get visitor
     *
     * @return integer
     */
    public function getVisitor()
    {
        return $this->visitor;
    }

    /**
     * Set visitor_group
     *
     * @param VisitorGroup $visitor_group
     * @return VisitorGroup
     */
    public function setVisitorGroup(VisitorGroup $visitor_group)
    {
        $this->visitor_group = $visitor_group;

        return $this;
    }

    /**
     * Get visitor_group
     *
     * @return integer
     */
    public function getVisitorGroup()
    {
        return $this->visitor_group;
    }


    /**
     * Set amount
     *
     * @param string $amount
     * @return Wallet
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set pincode
     *
     * @param integer $pincode
     * @return Wallet
     */
    public function setPincode($pincode)
    {
        $this->pincode = $pincode;

        return $this;
    }

    /**
     * Get pincode
     *
     * @return integer
     */
    public function getPincode()
    {
        return $this->pincode;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Wallet
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
