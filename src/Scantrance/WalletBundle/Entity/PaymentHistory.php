<?php

namespace Scantrance\WalletBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentHistory
 *
 * @ORM\Table(name="payment_history")
 * @ORM\Entity
 */
class PaymentHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal")
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var string
     *
     * @ORM\Column(name="bracelet_serial", type="string", length=255)
     */
    private $braceletSerial;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return PaymentHistory
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return PaymentHistory
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return PaymentHistory
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set braceletSerial
     *
     * @param string $braceletSerial
     * @return PaymentHistory
     */
    public function setBraceletSerial($braceletSerial)
    {
        $this->braceletSerial = $braceletSerial;

        return $this;
    }

    /**
     * Get braceletSerial
     *
     * @return string 
     */
    public function getBraceletSerial()
    {
        return $this->braceletSerial;
    }
}
