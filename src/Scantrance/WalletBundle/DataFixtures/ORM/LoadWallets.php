<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\WalletBundle\Entity\Wallet;
use Symfony\Component\Validator\Constraints\DateTime;

class LoadWallets extends AbstractFixture implements OrderedFixtureInterface  {

    public function load(ObjectManager $manager)
    {
        // Load Visitor group
        $group = $manager->getRepository("UserBundle:VisitorGroup");
        $group1 = $group->findOneBy(array('groupname' => 'teamyolo'));
        $group2 = $group->findOneBy(array('groupname' => 'teambeta'));


        // Load visitors from Customers
        $user = $manager->getRepository("UserBundle:User");
        $user1 = $user->findOneBy(array('username' => 'peterknijff'));
        $user4 = $user->findOneBy(array('username' => 'jorisphilipsen'));
        $user5 = $user->findOneBy(array('username' => 'henkdevries'));
        $user6 = $user->findOneBy(array('username' => 'ronakkermans'));


        $visitor = $manager->getRepository("UserBundle:Visitor");
        // Implement these visitors
        $visitor1 = $visitor->findOneBy(array('user' => $user1));
        $visitor4 = $visitor->findOneBy(array('user' => $user4));
        $visitor5 = $visitor->findOneBy(array('user' => $user5));
        $visitor6 = $visitor->findOneBy(array('user' => $user6));

        // Group wallet for user 1 and 2
        $wallet = new Wallet();
        $wallet->setAmount(255);
        $wallet->setPincode(1234);
        $wallet->setVisitorGroup($group1);
        $wallet->setCreated(new \DateTime('now'));
        $manager->persist($wallet);

        // Group wallet for user 3 and 4
        $wallet = new Wallet();
        $wallet->setAmount(150);
        $wallet->setPincode(1234);
        $wallet->setVisitorGroup($group2);
        $wallet->setCreated(new \DateTime('now'));
        $manager->persist($wallet);

        // Individual wallets for user 1, 3, 5 and 6
        $wallet = new Wallet();
        $wallet->setAmount(50);
        $wallet->setPincode(1234);
        $wallet->setVisitor($visitor1);
        $wallet->setCreated(new \DateTime('now'));
        $manager->persist($wallet);

        $wallet = new Wallet();
        $wallet->setAmount(25);
        $wallet->setPincode(1234);
        $wallet->setVisitor($visitor4);
        $wallet->setCreated(new \DateTime('now'));
        $manager->persist($wallet);

        $wallet = new Wallet();
        $wallet->setAmount(100);
        $wallet->setPincode(1234);
        $wallet->setVisitor($visitor5);
        $wallet->setCreated(new \DateTime('now'));
        $manager->persist($wallet);


        $wallet = new Wallet();
        $wallet->setAmount(50);
        $wallet->setPincode(1234);
        $wallet->setVisitor($visitor6);
        $wallet->setCreated(new \DateTime('now'));
        $manager->persist($wallet);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 20; // the order in which fixtures will be loaded
    }

}