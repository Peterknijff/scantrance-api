<?php

namespace Scantrance\WalletBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\WalletBundle\Entity\PaymentType;

class LoadPaymentTypes extends AbstractFixture implements OrderedFixtureInterface{

    public function load(ObjectManager $manager)
    {
        $paymentType = new PaymentType();
        $paymentType->setName('Balance debet');
        $paymentType->setDescription('Increasing ballance of wallet amount');
        $manager->persist($paymentType);

        $paymentType = new PaymentType();
        $paymentType->setName('Balance credit');
        $paymentType->setDescription('Decreasing ballance of wallet amount');
        $manager->persist($paymentType);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 21; // the order in which fixtures will be loaded
    }
}