<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19-04-15
 * Time: 22:31
 */

namespace Scantrance\UserBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Scantrance\UserBundle\Entity\User;
use Symfony\Component\Validator\ConstraintViolationList;

class UserController extends FOSRestController{

    /**
     * Return the overall user list.
     *
     * //@Secure(roles="ROLE_API")
     * @ApiDoc(
     *   resource = true,
     *   description = "Return the overall User List",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @return View
     */
    public function getUsersAction()
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $entity = $userManager->findUsers();

        if (!$entity) {
            throw $this->createNotFoundException('Data not found.');
        }

        $view = View::create();
        $view->setData($entity)->setStatusCode(200);

        return $view;
    }

    /**
     * Return an user identified by username/email.
     *
     * //@Secure(roles="ROLE_API")
     * @ApiDoc(
     *   resource = true,
     *   description = "Return an user identified by username/email",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @param string $slug username or email
     *
     * @return View
     */
    public function getUserAction($slug)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $entity = $userManager->findUserBy(array('username' => $slug));

        if (!$entity) {
            throw $this->createNotFoundException('Data not found.');
        }

        $view = View::create();
        $view->setData($entity)->setStatusCode(200);

        return $view;
    }

//    /**
//     * Create a User from the submitted data.<br/>
//     *
//     * @ApiDoc(
//     *   resource = true,
//     *   description = "Creates a new user from the submitted data.",
//     *   statusCodes = {
//     *     200 = "Returned when successful",
//     *     400 = "Returned when the form has errors"
//     *   }
//     * )
//     *
//     * @param ParamFetcher $paramFetcher Paramfetcher
//     *
//     * @RequestParam(name="firstname", nullable=false, strict=true, description="Firstname.")
//     * @RequestParam(name="lastname", nullable=false, strict=true, description="Lastname.")
//     * @RequestParam(name="email", nullable=false, strict=true, description="Email.")
//     * @RequestParam(name="plain_password", nullable=false, strict=true, description="Plain Password.")
//     *
//     * @return View
//     */
    public function postUserAction(ParamFetcher $paramFetcher)
    {

        $userManager = $this->container->get('fos_user.user_manager');

        $user = new User();
        $user->setFirstname($paramFetcher->get('firstname'));
        $user->setLastname($paramFetcher->get('lastname'));
        $user->setUsername($paramFetcher->get('firstname').$paramFetcher->get('lastname'));
        $user->setEmail($paramFetcher->get('email'));
        $user->setPlainPassword($paramFetcher->get('plain_password'));
        $user->setEnabled(true);

        $view = View::create();

        $errors = $this->get('validator')->validate($user, array('Registration'));

        if (count($errors) == 0) {
            $userManager->updateUser($user);
            $view->setData($user)->setStatusCode(200);
            return $view;
        } else {
            $view = $this->getErrorsView($errors);
            return $view;
        }
    }

//    /**
//     * Update a User from the submitted data by ID.<br/>
//     *
//     * //@Secure(roles="ROLE_API")
//     * @ApiDoc(
//     *   resource = true,
//     *   description = "Updates a user from the submitted data by ID.",
//     *   statusCodes = {
//     *     200 = "Returned when successful",
//     *     400 = "Returned when the form has errors"
//     *   }
//     * )
//     *
//     * @param ParamFetcher $paramFetcher Paramfetcher
//     *
//     * @RequestParam(name="id", nullable=false, strict=true, description="id.")
//     * @RequestParam(name="username", nullable=true, strict=true, description="Username.")
//     * @RequestParam(name="email", nullable=true, strict=true, description="Email.")
//     * @RequestParam(name="firstname", nullable=true, strict=true, description="Firstname.")
//     * @RequestParam(name="lastname", nullable=true, strict=true, description="Lastname.")
//     * @RequestParam(name="newsletter", nullable=true, strict=true, description="Newsletter.")
//     * @RequestParam(name="password", nullable=true, strict=true, description="Plain Password.")
//     *
//     * @return View
//     */
    public function putUserAction(ParamFetcher $paramFetcher)
    {

        $entity = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(
            array('id' => $paramFetcher->get('id'))
        );

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($entity->getUsername());

        if($paramFetcher->get('username')){ $user->setUsername($paramFetcher->get('username')); }
        if($paramFetcher->get('email')){$user->setEmail($paramFetcher->get('email')); }
        if($paramFetcher->get('password')){$user->setPlainPassword($paramFetcher->get('password')); }
        if($paramFetcher->get('firstname')){$user->setF($paramFetcher->get('firstname')); }
        if($paramFetcher->get('lastname')){$user->setLastname($paramFetcher->get('lastname')); }

        $view = View::create();

        $errors = $this->get('validator')->validate($user, array('Update'));

        if (count($errors) == 0) {
            $userManager->updateUser($user);
            $view->setData($user)->setStatusCode(200);
            return $view;
        } else {
            $view = $this->getErrorsView($errors);
            return $view;
        }
    }


//    /**
//     * Get the validation errors
//     *
//     * @param ConstraintViolationList $errors Validator error list
//     *
//     * @return View
//     */
    protected function getErrorsView(ConstraintViolationList $errors)
    {
        $msgs = array();
        $errorIterator = $errors->getIterator();
        foreach ($errorIterator as $validationError) {
            $msg = $validationError->getMessage();
            $params = $validationError->getMessageParameters();
            $msgs[$validationError->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = View::create($msgs);
        $view->setStatusCode(400);

        return $view;
    }


}