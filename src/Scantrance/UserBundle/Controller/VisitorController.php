<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 19-04-15
 * Time: 22:31
 */

namespace Scantrance\UserBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Scantrance\UserBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;

class VisitorController extends FOSRestController{

    /**
     * Update a User if neccessary, checks:<br/>
     * - Serial validation.<br/>
     * - Checkin status.<br/>
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Updates the status of the bracelet when checkpoints are completely passed.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="serial", nullable=false, strict=true, description="Serial of the bracelet.")
     *
     * @return View
     */
    public function putVisitorCheckinAction(ParamFetcher $paramFetcher)
    {

        $bracelet = $this->getDoctrine()->getRepository('UserBundle:Bracelet')->findOneBy(
            array('serial' => $paramFetcher->get('serial'))
        );

        // If bracelet exists check in.
        if($bracelet != null){


            if($bracelet->getCheckedIn() == false) {
                // Check in bracelet
                $manager = $this->getDoctrine()->getManager();
                $bracelet->setCheckedIn(true);
                $manager->persist($bracelet);

                $manager->flush();
                throw new HttpException(200, "checked_in");
            }else{
                throw new HttpException(400, "allready_checkedin");
            }
        }else{
            throw new HttpException(400, "not_exist");
        }
    }


//    /**
//     * Get the validation errors
//     *
//     * @param ConstraintViolationList $errors Validator error list
//     *
//     * @return View
//     */
    protected function getErrorsView(ConstraintViolationList $errors)
    {
        $msgs = array();
        $errorIterator = $errors->getIterator();
        foreach ($errorIterator as $validationError) {
            $msg = $validationError->getMessage();
            $params = $validationError->getMessageParameters();
            $msgs[$validationError->getPropertyPath()][] = $this->get('translator')->trans($msg, $params, 'validators');
        }
        $view = View::create($msgs);
        $view->setStatusCode(400);

        return $view;
    }


}