<?php

namespace Scantrance\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bracelet
 *
 * @ORM\Table(name="bracelet")
 * @ORM\Entity
 */
class Bracelet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Scantrance\UserBundle\Entity\BraceletType", inversedBy="bracelet")
     *
     * @ORM\JoinColumn(name="bracelet_type_id", referencedColumnName="id")
     *
     **/
    private $bracelet_type;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255)
     */
    private $serial;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp_link", type="datetime")
     */
    private $timestampLink;

    /**
     * @var boolean
     *
     * @ORM\Column(name="checked_in", type="boolean")
     */
    private $checked_in;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bracelet_type
     *
     * @param BraceletType $bracelet_type
     * @return Bracelet
     */
    public function setBraceletType(BraceletType $bracelet_type)
    {
        $this->bracelet_type = $bracelet_type;

        return $this;
    }

    /**
     * Get bracelet_type
     *
     * @return BraceletType
     */
    public function getBraceletType()
    {
        return $this->bracelet_type;
    }

    /**
     * Set serial
     *
     * @param string $serial
     * @return Bracelet
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get serial
     *
     * @return string 
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set timestampLink
     *
     * @param \DateTime $timestampLink
     * @return Bracelet
     */
    public function setTimestampLink($timestampLink)
    {
        $this->timestampLink = $timestampLink;

        return $this;
    }

    /**
     * Get timestampLink
     *
     * @return \DateTime 
     */
    public function getTimestampLink()
    {
        return $this->timestampLink;
    }

    /**
     * Set checked_in
     *
     * @param boolean $checkedIn
     * @return Bracelet
     */
    public function setCheckedIn($checkedIn)
    {
        $this->checked_in = $checkedIn;

        return $this;
    }

    /**
     * Get checked_in
     *
     * @return boolean
     */
    public function getCheckedIn()
    {
        return $this->checked_in;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Bracelet
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
