<?php

namespace Scantrance\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserOrder
 *
 * @ORM\Table(name="user_order")
 * @ORM\Entity
 */
class UserOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Scantrance\UserBundle\Entity\User", inversedBy="user_order")
     *
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     *
     **/
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="amount_excl", type="decimal")
     */
    private $amountExcl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set user
     *
     * @param User $user
     * @return UserOrder
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set amountExcl
     *
     * @param string $amountExcl
     * @return UserOrder
     */
    public function setAmountExcl($amountExcl)
    {
        $this->amountExcl = $amountExcl;

        return $this;
    }

    /**
     * Get amountExcl
     *
     * @return string 
     */
    public function getAmountExcl()
    {
        return $this->amountExcl;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return UserOrder
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}
