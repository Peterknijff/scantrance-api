<?php

namespace Scantrance\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * User
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity()
 *
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\OneToMany(targetEntity="Scantrance\UserBundle\Entity\UserOrder", mappedBy="user")
     * @ORM\JoinColumn(name="user_order_id", referencedColumnName="id")
     **/
    private $user_order;

    public function __construct()
    {
        parent::__construct();
        $this->user_order = new ArrayCollection();
    }

    /**
     * @ORM\OneToOne(targetEntity="Scantrance\UserBundle\Entity\Visitor", mappedBy="user")
     **/
    private $visitor;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    protected $lastname;

    /**
     * @var integer
     *
     * @ORM\Column(name="newsletter", type="smallint", nullable=true)
     */
    protected $newsletter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    protected $created;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add userOrder
     *
     * @param UserOrder $user_order
     * @return User
     */
    public function addUserOrder(UserOrder $user_order)
    {
        $this->user_order[] = $user_order;

        return $this;
    }

    /**
     * Remove userOrder
     *
     * @param UserOrder $user_order
     */
    public function removeUserOrder(UserOrder $user_order)
    {
        $this->user_order->removeElement($user_order);
    }

    /**
     * Get userOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserOrder()
    {
        return $this->user_order;
    }

    /**
     * Set visitor
     *
     * @param Visitor $visitor
     * @return Visitor
     */
    public function setVisitor(Visitor $visitor)
    {
        $this->visitor = $visitor;

        return $this;
    }

    /**
     * Get visitor
     *
     * @return integer
     */
    public function getVisitor()
    {
        return $this->visitor;
    }


    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set newsletter
     *
     * @param integer $newsletter
     * @return User
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return integer
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

}
