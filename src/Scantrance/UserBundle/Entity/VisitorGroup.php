<?php

namespace Scantrance\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use Scantrance\WalletBundle\Entity\Wallet;

/**
 * VisitorGroup
 *
 * @ORM\Table(name="visitor_group")
 * @ORM\Entity
 */
class VisitorGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Exclude
     * @ORM\OneToMany(targetEntity="Visitor", mappedBy="visitor_group")
     * @ORM\JoinColumn(name="visitor_id", referencedColumnName="id")
     **/
    private $visitor;

    public function __construct()
    {
        $this->visitor = new ArrayCollection();
    }

    /**
     * @ORM\OneToOne(targetEntity="Scantrance\WalletBundle\Entity\Wallet", mappedBy="visitor_group")
     **/
    private $wallet;

    /**
     * @var string
     *
     * @ORM\Column(name="groupname", type="string", length=255)
     */
    private $groupname;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add visitor
     *
     * @param Visitor $visitor
     * @return VisitorGroup
     */
    public function addVisitor(Visitor $visitor)
    {
        $this->visitor[] = $visitor;

        return $this;
    }

    /**
     * Remove visitor
     *
     * @param Visitor $visitor
     */
    public function removeVisitor(Visitor $visitor)
    {
        $this->visitor->removeElement($visitor);
    }

    /**
     * Get visitor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVisitor()
    {
        return $this->visitor;
    }

    /**
     * Set wallet
     *
     * @param Wallet $wallet
     * @return Wallet
     */
    public function setWallet(Wallet $wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * Get wallet
     *
     * @return integer
     */
    public function getWallet()
    {
        return $this->wallet;
    }


    /**
     * Set groupname
     *
     * @param string $groupname
     * @return VisitorGroup
     */
    public function setGroupName($groupname)
    {
        $this->groupname = $groupname;

        return $this;
    }

    /**
     * Get groupname
     *
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupname;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return VisitorGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return VisitorGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
