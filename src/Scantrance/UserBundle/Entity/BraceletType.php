<?php

namespace Scantrance\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * BraceletType
 *
 * @ORM\Table(name="bracelet_type")
 * @ORM\Entity
 */
class BraceletType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Exclude
     * @ORM\OneToMany(targetEntity="Scantrance\UserBundle\Entity\Bracelet", mappedBy="bracelet_type")
     * @ORM\JoinColumn(name="bracelet_id", referencedColumnName="id")
     **/
    private $bracelet;

    public function __construct()
    {
        $this->bracelet = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add bracelet
     *
     * @param Bracelet $bracelet
     * @return BraceletType
     */
    public function addBracelet(Bracelet $bracelet)
    {
        $this->bracelet[] = $bracelet;

        return $this;
    }

    /**
     * Remove bracelet
     *
     * @param Bracelet $bracelet
     */
    public function removeBracelet(Bracelet $bracelet)
    {
        $this->bracelet->removeElement($bracelet);
    }

    /**
     * Get bracelet
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBracelet()
    {
        return $this->bracelet;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BraceletType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BraceletType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
