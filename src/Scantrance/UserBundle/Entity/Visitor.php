<?php

namespace Scantrance\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use Scantrance\GameBundle\Entity\Scoreboard;
use Scantrance\WalletBundle\Entity\Wallet;

/**
 * Visitor
 *
 * @ORM\Table(name="visitor")
 * @ORM\Entity
 */
class Visitor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Exclude
     * @ORM\OneToOne(targetEntity="User", inversedBy="visitor")
     *
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     *
     **/
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="Scantrance\WalletBundle\Entity\Wallet", mappedBy="visitor")
     **/
    private $wallet;

    /**
     *
     * @ORM\OneToOne(targetEntity="Bracelet")
     *
     * @ORM\JoinColumn(name="bracelet_id", referencedColumnName="id")
     *
     **/
    private $bracelet;

    /**
     *
     * @ORM\ManyToOne(targetEntity="VisitorGroup", inversedBy="visitor")
     *
     * @ORM\JoinColumn(name="visitor_group_id", referencedColumnName="id")
     *
     **/
    private $visitor_group;


    /**
     *
     * @ORM\OneToMany(targetEntity="Scantrance\GameBundle\Entity\Scoreboard", mappedBy="visitor")
     * @ORM\JoinColumn(name="scoreboard_id", referencedColumnName="id")
     **/
    private $scoreboard;

    public function __construct()
    {
        $this->scoreboard = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return User
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set wallet
     *
     * @param Wallet $wallet
     * @return Wallet
     */
    public function setWallet(Wallet $wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * Get wallet
     *
     * @return integer
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * Set bracelet
     *
     * @param Bracelet $bracelet
     * @return Bracelet
     */
    public function setBracelet(Bracelet $bracelet)
    {
        $this->bracelet = $bracelet;

        return $this;
    }

    /**
     * Get bracelet
     *
     * @return integer
     */
    public function getBracelet()
    {
        return $this->bracelet;
    }

    /**
     * Set visitor_group
     *
     * @param VisitorGroup $visitor_group
     * @return Visitor
     */
    public function setVisitorGroup(VisitorGroup $visitor_group = null)
    {
        $this->visitor_group = $visitor_group;

        return $this;
    }

    /**
     * Get visitor_group
     *
     * @return VisitorGroup
     */
    public function getVisitorGroup()
    {
        return $this->visitor_group;
    }

    /**
     * Add scoreboard
     *
     * @param Scoreboard $scoreboard
     * @return Visitor
     */
    public function addScoreboard(Scoreboard $scoreboard)
    {
        $this->scoreboard[] = $scoreboard;

        return $this;
    }

    /**
     * Remove scoreboard
     *
     * @param Scoreboard $scoreboard
     */
    public function removeScoreboard(Scoreboard $scoreboard)
    {
        $this->scoreboard->removeElement($scoreboard);
    }

    /**
     * Get scoreboard
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScoreboard()
    {
        return $this->scoreboard;
    }

}
