<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\UserBundle\Entity\VisitorGroup;

class LoadVisitorGroups extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        // Visitors who are in a group
        $visitor_group = new VisitorGroup();
        $visitor_group->setName('Team YOLO');
        $visitor_group->setGroupName('teamyolo');
        $visitor_group->setDescription('Team Yolo de gekste! Uit brabant jonguh! BOTS!');
        $manager->persist($visitor_group);


        // Visitors who are in a group
        $visitor_group = new VisitorGroup();
        $visitor_group->setName('Team BETA');
        $visitor_group->setGroupName('teambeta');
        $visitor_group->setDescription('Team Beta gaat voor de overwinning! Challenge accepted.');
        $manager->persist($visitor_group);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10; // the order in which fixtures will be loaded
    }
}