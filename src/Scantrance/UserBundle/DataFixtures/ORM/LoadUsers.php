<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\UserBundle\Entity\User;

class LoadUsers extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setFirstname('Peter');
        $userAdmin->setLastname('Knijff');
        $userAdmin->setUsername('peterknijff');
        $userAdmin->setEmail('pk@hro.nl');
        $userAdmin->setPlainPassword('test123');
        $userAdmin->setCreated(new \DateTime('now'));
        $userAdmin->setRoles(array('ROLE_ADMIN', 'ROLE_API'));
        $userAdmin->setEnabled(true);
        $manager->persist($userAdmin);

        $user = new User();
        $user->setFirstname('Shailin');
        $user->setLastname('Biharie');
        $user->setUsername('shailinbiharie');
        $user->setEmail('sb@hro.nl');
        $user->setPlainPassword('test123');
        $user->setCreated(new \DateTime('now'));
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setFirstname('Stephan');
        $user->setLastname('Tromer');
        $user->setUsername('stephantromer');
        $user->setEmail('st@hro.nl');
        $user->setPlainPassword('test123');
        $user->setCreated(new \DateTime('now'));
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setFirstname('Joris');
        $user->setLastname('Philipsen');
        $user->setUsername('jorisphilipsen');
        $user->setEmail('js@hro.nl');
        $user->setPlainPassword('test123');
        $user->setCreated(new \DateTime('now'));
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setFirstname('Henk');
        $user->setLastname('de Vries');
        $user->setUsername('henkdevries');
        $user->setEmail('hdv@hro.nl');
        $user->setPlainPassword('test123');
        $user->setCreated(new \DateTime('now'));
        $user->setEnabled(true);
        $manager->persist($user);

        $user = new User();
        $user->setFirstname('Ron');
        $user->setLastname('Akkermans');
        $user->setUsername('ronakkermans');
        $user->setEmail('ra@hro.nl');
        $user->setPlainPassword('test123');
        $user->setCreated(new \DateTime('now'));
        $user->setEnabled(true);
        $manager->persist($user);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 12; // the order in which fixtures will be loaded
    }
}