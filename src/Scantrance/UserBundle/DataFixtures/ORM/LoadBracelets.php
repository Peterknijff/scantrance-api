<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\UserBundle\Entity\Bracelet;

class LoadBracelets extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $bracelet_type = $manager->getRepository("UserBundle:BraceletType");
        $bracelet_type = $bracelet_type->findOneBy(array('name' => 'NFC'));

        $bracelet = new Bracelet();
        $bracelet->setBraceletType($bracelet_type);
        $bracelet->setSerial('0271778560');
        $bracelet->setTimestampLink(new \DateTime('now'));
        $bracelet->setCheckedIn(false);
        $bracelet->setEnabled(true);
        $manager->persist($bracelet);

        $bracelet = new Bracelet();
        $bracelet->setBraceletType($bracelet_type);
        $bracelet->setSerial('0001573632');
        $bracelet->setTimestampLink(new \DateTime('now'));
        $bracelet->setCheckedIn(false);
        $bracelet->setEnabled(true);
        $manager->persist($bracelet);

        $bracelet = new Bracelet();
        $bracelet->setBraceletType($bracelet_type);
        $bracelet->setSerial('qq:21:a2:25');
        $bracelet->setTimestampLink(new \DateTime('now'));
        $bracelet->setCheckedIn(false);
        $bracelet->setEnabled(true);
        $manager->persist($bracelet);

        $bracelet = new Bracelet();
        $bracelet->setBraceletType($bracelet_type);
        $bracelet->setSerial('1w:d1:a2:20');
        $bracelet->setTimestampLink(new \DateTime('now'));
        $bracelet->setCheckedIn(false);
        $bracelet->setEnabled(true);
        $manager->persist($bracelet);

        $bracelet = new Bracelet();
        $bracelet->setBraceletType($bracelet_type);
        $bracelet->setSerial('3225494272');
        $bracelet->setTimestampLink(new \DateTime('now'));
        $bracelet->setCheckedIn(false);
        $bracelet->setEnabled(true);
        $manager->persist($bracelet);

        $bracelet = new Bracelet();
        $bracelet->setBraceletType($bracelet_type);
        $bracelet->setSerial('1x:ss:ss:qw');
        $bracelet->setTimestampLink(new \DateTime('now'));
        $bracelet->setCheckedIn(false);
        $bracelet->setEnabled(true);
        $manager->persist($bracelet);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11; // the order in which fixtures will be loaded
    }
}