<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\UserBundle\Entity\User;
use Scantrance\UserBundle\Entity\Visitor;

class LoadVisitors extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $bracelet = $manager->getRepository("UserBundle:Bracelet");
        $bracelet1 = $bracelet->findOneBy(array('serial' => '0271778560'));
        $bracelet2 = $bracelet->findOneBy(array('serial' => '0001573632'));
        $bracelet3 = $bracelet->findOneBy(array('serial' => 'qq:21:a2:25'));
        $bracelet4 = $bracelet->findOneBy(array('serial' => '1w:d1:a2:20'));
        $bracelet5 = $bracelet->findOneBy(array('serial' => '3225494272'));
        $bracelet6 = $bracelet->findOneBy(array('serial' => '1x:ss:ss:qw'));

        $user = $manager->getRepository("UserBundle:User");
        $user1 = $user->findOneBy(array('username' => 'peterknijff'));
        $user2 = $user->findOneBy(array('username' => 'shailinbiharie'));
        $user3 = $user->findOneBy(array('username' => 'stephantromer'));
        $user4 = $user->findOneBy(array('username' => 'jorisphilipsen'));
        $user5 = $user->findOneBy(array('username' => 'henkdevries'));
        $user6 = $user->findOneBy(array('username' => 'ronakkermans'));

        $visitor_group = $manager->getRepository("UserBundle:VisitorGroup");
        $group1 = $visitor_group->findOneBy(array('groupname' => 'teamyolo'));
        $group2 = $visitor_group->findOneBy(array('groupname' => 'teambeta'));

        // Visitors who are in a group
        // TEAM YOLO
        $visitor = new Visitor();
        $visitor->setBracelet($bracelet1);
        $visitor->setUser($user1);
        $visitor->setVisitorGroup($group1);
        $manager->persist($visitor);

        $visitor = new Visitor();
        $visitor->setBracelet($bracelet2);
        $visitor->setUser($user2);
        $visitor->setVisitorGroup($group1);
        $manager->persist($visitor);

        // TEAM BETA
        $visitor = new Visitor();
        $visitor->setBracelet($bracelet3);
        $visitor->setUser($user3);
        $visitor->setVisitorGroup($group2);
        $manager->persist($visitor);

        $visitor = new Visitor();
        $visitor->setBracelet($bracelet4);
        $visitor->setUser($user4);
        $visitor->setVisitorGroup($group2);
        $manager->persist($visitor);

        // Visitors who are not in a group
        $visitor = new Visitor();
        $visitor->setBracelet($bracelet5);
        $visitor->setUser($user5);
        $manager->persist($visitor);

        $visitor = new Visitor();
        $visitor->setBracelet($bracelet6);
        $visitor->setUser($user6);
        $manager->persist($visitor);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 13; // the order in which fixtures will be loaded
    }
}