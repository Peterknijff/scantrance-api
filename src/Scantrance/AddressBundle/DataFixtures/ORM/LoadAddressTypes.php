<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\AddressBundle\Entity\AddressType;

class LoadAddressTypes extends AbstractFixture implements OrderedFixtureInterface{

    public function load(ObjectManager $manager)
    {
        $addressType = new AddressType();
        $addressType->setName('Personal invoice address');
        $addressType->setType('personal_invoice');
        $addressType->setDescription('Invoice address of customer');
        $manager->persist($addressType);

        $addressType = new AddressType();
        $addressType->setName('Business invoice address');
        $addressType->setType('business_invoice');
        $addressType->setDescription('Invoice address of the company');
        $manager->persist($addressType);

        $addressType = new AddressType();
        $addressType->setName('Personal shipment address');
        $addressType->setType('personal_shipment');
        $addressType->setDescription('Shipment address of customer');
        $manager->persist($addressType);

        $addressType = new AddressType();
        $addressType->setName('Business shipment address');
        $addressType->setType('business_shipment');
        $addressType->setDescription('Shipment address of the company');
        $manager->persist($addressType);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}