<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\AddressBundle\Entity\Address;


class LoadAddresses extends AbstractFixture implements OrderedFixtureInterface  {

    public function load(ObjectManager $manager)
    {
        $company = $manager->getRepository("AddressBundle:Company");
        $company = $company->findOneBy(array('name' => 'HRO'));

        $address_type = $manager->getRepository("AddressBundle:AddressType");
        $address_personal_invoice = $address_type->findOneBy(array('type' => 'personal_invoice'));
        $address_business_invoice = $address_type->findOneBy(array('type' => 'business_invoice'));

        $event = new Address();
        $event->setStreetname('Hooidrift');
        $event->setHousenumber('155B02');
        $event->setZipcode('3023KM');
        $event->setCity('Rotterdam');
        $event->setCountry('Holland');
        $event->setPhone('+31646408646');
        $event->setAddressType($address_personal_invoice);
        $manager->persist($event);

        $event = new Address();
        $event->setStreetname('Wijnhaven');
        $event->setHousenumber('99');
        $event->setZipcode('3013KS');
        $event->setCity('Rotterdam');
        $event->setCountry('Holland');
        $event->setPhone('+3101012345687');
        $event->setAddressType($address_business_invoice);
        $event->setCompany($company);

        $manager->persist($event);


        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }

}