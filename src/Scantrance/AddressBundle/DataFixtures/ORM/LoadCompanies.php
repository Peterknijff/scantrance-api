<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\AddressBundle\Entity\Company;

class LoadCompanies extends AbstractFixture implements OrderedFixtureInterface  {

    public function load(ObjectManager $manager)
    {
        $company = new Company();
        $company->setName('HRO');
        $company->setContactPerson('Peter Knijff');
        $company->setEmail('pk@hro.nl');
        $company->setPhone('0646408646');
        $manager->persist($company);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

}