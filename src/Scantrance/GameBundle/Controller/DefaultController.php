<?php

namespace Scantrance\GameBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function indexAction()
    {
        return $this->render('GameBundle:Default:team.html.twig');
    }

    public function teamAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('UserBundle:User');
        // get all categories where subcategory is null
        $users = $repo->findAll();

//        $gameRepo = $em->getRepository('GameBundle:Scoreboard');
//
//
//        foreach($users as $user){
//            $visitor = $user->getVisitor();
//            $games = $gameRepo->findBy(array('visitor' => $visitor));
//            foreach($games as $game){
//                $playedGames = $game->getGames();
//                foreach($playedGames as $playedGame){
//                }
//            }
//        }





        return $this->render('@Game/Default/team.html.twig', array('users' => $users));

    }

    public function singleAction()
    {
        return $this->render('GameBundle:Default:single.html.twig');
    }


}
