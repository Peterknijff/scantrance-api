<?php

namespace Scantrance\GameBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Scantrance\GameBundle\Entity\Game;
use Scantrance\GameBundle\Entity\Scoreboard;
use Scantrance\UserBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;

class ScoreboardController extends FOSRestController{

    /**
     * When a game is played, the visitor will get a new scoreboard item with the game he played:<br/>
     * - Serial validation.<br/>
     * - Game Type.<br/>
     *
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Posts new scoreboard item with specific played game.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param ParamFetcher $paramFetcher Paramfetcher
     *
     * @RequestParam(name="serial", nullable=false, strict=true, description="Serial of the bracelet.")
     * @RequestParam(name="reference", nullable=false, strict=true, description="Refrence of the game.")
     *
     * @return View
     */
    public function postScoreboardAction(ParamFetcher $paramFetcher)
    {
        $bracelet = $this->getDoctrine()->getRepository('UserBundle:Bracelet')->findOneBy(
            array('serial' => $paramFetcher->get('serial'))
        );

        $visitor = $this->getDoctrine()->getRepository('UserBundle:Visitor')->findOneBy(
            array('bracelet' => $bracelet)
        );

        $game = $this->getDoctrine()->getRepository('GameBundle:Game')->findOneBy(
            array('reference' => $paramFetcher->get('reference'))
        );

        // If bracelet and game exist post to scoreboard.
        if($bracelet != null && $game != null){
            if($bracelet->getEnabled()) {
                //Update the user for a specific game
                $manager = $this->getDoctrine()->getManager();
                $scoreboard = new Scoreboard();
                $scoreboard->setVisitor($visitor);
                if($visitor->getVisitorGroup() != null){
                    $scoreboard->setInGroup(true);
                }
                $scoreboard->setGame($game);
                $scoreboard->setTimestamp(new \DateTime('now'));
                $manager->persist($scoreboard);

                $manager->persist($bracelet);
                $manager->flush();
                throw new HttpException(200, "success");
            }else{
                throw new HttpException(400, "error");
            }
        }else{
            throw new HttpException(400, "not_exist");
        }
    }
//
//    /**
//     * Get recent scoreboard of all teams<br/>
//     *
//     * @ApiDoc(
//     *   resource = true,
//     *   description = "Get scoreboard items.",
//     *   statusCodes = {
//     *     200 = "Returned when successful",
//     *     400 = "Returned when the form has errors"
//     *   }
//     * )
//     *
//     *
//     * @return View
//     */
//    public function getScoreboardTeamAction()
//    {
//
//
//        $userManager = $this->container->get('fos_user.user_manager');
//        $entity = $userManager->findUsers();
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Data not found.');
//        }
//
//        $view = View::create();
//        $view->setData($entity)->setStatusCode(200);
//
//        return $view;
//
//
//    }


    public function getScoreboardTeamAction()
    {

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('GameBundle:Scoreboard');
        // get all categories where subcategory is null
        $teams = $repo->findBy(
            array(
                'position' => 'ASC'
            ));

        return $this->render('@Game/Default/team.html.twig', array('teams' => $teams));
    }


}