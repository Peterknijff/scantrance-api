<?php

namespace Scantrance\GameBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Scantrance\UserBundle\Entity\Visitor;

/**
 * Scoreboard
 *
 * @ORM\Table(name="scoreboard")
 * @ORM\Entity
 */
class Scoreboard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Scantrance\UserBundle\Entity\Visitor", inversedBy="scoreboard")
     *
     * @ORM\JoinColumn(name="visitor_id", referencedColumnName="id", nullable=false)
     *
     **/
    private $visitor;

    /**
     * @ORM\ManyToMany(targetEntity="Scantrance\GameBundle\Entity\Game")
     * @ORM\JoinTable(name="scoreboard_game",
     *      joinColumns={@ORM\JoinColumn(name="scoreboard_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="id")}
     *      )
     **/
    private $games;

    public function __construct() {
        $this->games = new ArrayCollection();
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="in_group", type="boolean")
     */
    private $inGroup;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visitor
     *
     * @param Visitor $visitor
     * @return Scoreboard
     */
    public function setVisitor(Visitor $visitor)
    {
        $this->visitor = $visitor;

        return $this;
    }

    /**
     * Get visitor
     *
     * @return Visitor
     */
    public function getVisitor()
    {
        return $this->visitor;
    }

    /**
     * Add game
     *
     * @param Game $game
     */
    public function setGame(Game $game)
    {
        $this->games[] = $game;
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Scoreboard
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set inGroup
     *
     * @param boolean $inGroup
     * @return Scoreboard
     */
    public function setInGroup($inGroup)
    {
        $this->inGroup = $inGroup;

        return $this;
    }

    /**
     * Get inGroup
     *
     * @return boolean 
     */
    public function getInGroup()
    {
        return $this->inGroup;
    }
}
