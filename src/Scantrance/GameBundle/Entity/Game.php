<?php

namespace Scantrance\GameBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity
 */
class Game
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="GameType", inversedBy="game")
     *
     * @ORM\JoinColumn(name="game_type_id", referencedColumnName="id")
     *
     **/
    private $game_type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="points_group", type="integer")
     */
    private $pointsGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="points_single", type="integer")
     */
    private $pointsSingle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set game_type
     *
     * @param GameType $game_type
     * @return Game
     */
    public function setGameType(GameType $game_type)
    {
        $this->game_type = $game_type;

        return $this;
    }

    /**
     * Get game_type
     *
     * @return GameType
     */
    public function getGameType()
    {
        return $this->game_type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Game
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set pointsGroup
     *
     * @param integer $pointsGroup
     * @return Game
     */
    public function setPointsGroup($pointsGroup)
    {
        $this->pointsGroup = $pointsGroup;

        return $this;
    }

    /**
     * Get pointsGroup
     *
     * @return integer 
     */
    public function getPointsGroup()
    {
        return $this->pointsGroup;
    }

    /**
     * Set pointsSingle
     *
     * @param integer $pointsSingle
     * @return Game
     */
    public function setPointsSingle($pointsSingle)
    {
        $this->pointsSingle = $pointsSingle;

        return $this;
    }

    /**
     * Get pointsSingle
     *
     * @return integer
     */
    public function getPointsSingle()
    {
        return $this->pointsSingle;
    }
}
