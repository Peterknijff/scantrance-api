<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\GameBundle\Entity\Game;

class LoadGames extends AbstractFixture implements OrderedFixtureInterface  {

    public function load(ObjectManager $manager)
    {

        $game_type = $manager->getRepository("GameBundle:GameType");
        $game_type1 = $game_type->findOneBy(array('name' => 'Quest'));
        $game_type2 = $game_type->findOneBy(array('name' => 'Duel'));


        $game = new Game();
        $game->setName('Zakdoek Quest');
        $game->setReference('zakdoek_quest');
        $game->setGameType($game_type1);
        $game->setPointsGroup(80);
        $game->setPointsSingle(100);
        $game->setDescription('Speler(s) moet(en) een rode zakdoek vinden en inleveren om te winnen.');
        $manager->persist($game);

        $game = new Game();
        $game->setName('Beerpong');
        $game->setReference('beerpong');
        $game->setGameType($game_type2);
        $game->setPointsGroup(0);
        $game->setPointsSingle(80);
        $game->setDescription('Twee spelers spelen tegen elkaar door een ping pong balletje in bekertjes van de tegenstander te gooien!');
        $manager->persist($game);

        $game = new Game();
        $game->setName('7 tegen 7 voetbaltoernooi');
        $game->setReference('voetbal');
        $game->setGameType($game_type2);
        $game->setPointsGroup(100);
        $game->setPointsSingle(0);
        $game->setDescription('2 teams van elk 7 man spelen een voetbaltoernooi tegen elkaar!');
        $manager->persist($game);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 18; // the order in which fixtures will be loaded
    }

}