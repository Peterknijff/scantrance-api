<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Scantrance\GameBundle\Entity\GameEnrolement;

class LoadGameEnrolements extends AbstractFixture implements OrderedFixtureInterface  {

    public function load(ObjectManager $manager)
    {

        $event = new GameEnrolement();
        $event->setType('Quest');
        $event->setDescription('Bezoeker volgt een quest, een zogeheten opdracht die uitgevoerd moet worden.');
        $manager->persist($event);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 17; // the order in which fixtures will be loaded
    }

}