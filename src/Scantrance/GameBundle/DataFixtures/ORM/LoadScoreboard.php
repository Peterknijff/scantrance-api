<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\GameBundle\Entity\GameType;
use Scantrance\GameBundle\Entity\Scoreboard;

class LoadScoreboard extends AbstractFixture implements OrderedFixtureInterface{

    public function load(ObjectManager $manager)
    {
//        $user = $manager->getRepository("UserBundle:User");
//        $user1 = $user->findOneBy(array('username' => 'peterknijff'));
//        $user2 = $user->findOneBy(array('username' => 'jorisphilipsen'));
//        $user3 = $user->findOneBy(array('username' => 'shailinbiharie'));
//
//        $visitor = $manager->getRepository("UserBundle:Visitor");
//        $visitor1 = $visitor->findOneBy(array('user' => $user1));
//        $visitor2 = $visitor->findOneBy(array('user' => $user2));
//        $visitor3 = $visitor->findOneBy(array('user' => $user3));
//
//        $game = $manager->getRepository("GameBundle:Game");
//        $game1 = $game->findOneBy(array('reference' => 'zakdoek_quest'));
//        $game2 = $game->findOneBy(array('reference' => 'beerpong'));
//        $game3 = $game->findOneBy(array('reference' => 'voetbal'));
//
//        $scoreboard = new Scoreboard();
//        $scoreboard->setVisitor($visitor1);
//        $scoreboard->setGame($game1);
//        $scoreboard->setTimestamp(new \DateTime('now'));
//        $scoreboard->setInGroup(false);
//        $manager->persist($scoreboard);
//
//        $scoreboard = new Scoreboard();
//        $scoreboard->setVisitor($visitor1);
//        $scoreboard->setGame($game2);
//        $scoreboard->setTimestamp(new \DateTime('now'));
//        $scoreboard->setInGroup(false);
//        $manager->persist($scoreboard);
//
//        $scoreboard = new Scoreboard();
//        $scoreboard->setVisitor($visitor2);
//        $scoreboard->setGame($game3);
//        $scoreboard->setTimestamp(new \DateTime('now'));
//        $scoreboard->setInGroup(true);
//        $manager->persist($scoreboard);
//
//        $scoreboard = new Scoreboard();
//        $scoreboard->setVisitor($visitor3);
//        $scoreboard->setGame($game3);
//        $scoreboard->setTimestamp(new \DateTime('now'));
//        $scoreboard->setInGroup(true);
//        $manager->persist($scoreboard);
//
//
//        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 19; // the order in which fixtures will be loaded
    }
}