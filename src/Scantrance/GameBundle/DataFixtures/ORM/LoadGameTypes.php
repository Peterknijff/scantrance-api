<?php

namespace Scantrance\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Scantrance\GameBundle\Entity\GameType;

class LoadGameTypes extends AbstractFixture implements OrderedFixtureInterface{

    public function load(ObjectManager $manager)
    {
        $gametype = new GameType();
        $gametype->setName('Quest');
        $gametype->setDescription('Het uitzoeken van een vraag, of iets zoeken');
        $manager->persist($gametype);

        $gametype = new GameType();
        $gametype->setName('Race');
        $gametype->setDescription('Tegen elkaar racen');
        $manager->persist($gametype);

        $gametype = new GameType();
        $gametype->setName('Duel');
        $gametype->setDescription('Tegen elkaar strijden');
        $manager->persist($gametype);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 16; // the order in which fixtures will be loaded
    }
}