////////////////////////////////////////////////////////////////////////
//INCLUDES
////////////////////////////////////////////////////////////////////////
#include <PN532.h>
#include <SPI.h>
#include <Ethernet.h>
#include <aJSON.h>

////////////////////////////////////////////////////////////////////////
// DEFINITIONS
////////////////////////////////////////////////////////////////////////
#define NFC_SS 9
#define ETH_SS 10
PN532 nfc(NFC_SS);
EthernetClient client;

////////////////////////////////////////////////////////////////////////
// PROGRAM STATES
////////////////////////////////////////////////////////////////////////
#define STATE_IDDLE 0
#define STATE_SENDDATA 15
#define STATE_RESPONSE 20

////////////////////////////////////////////////////////////////////////
// VARIABLES
///////////////////////////////////////////////////////////////////////
bool reading = false;
bool proceed = true;
bool gTrue = false;
bool valueTrue = false;
String strResponse = "";
int x = 0;
uint32_t tagId=0;
uint32_t xmitId=0;
uint32_t flowState =0;
char  tagIdString [11]= "1234567890";

////////////////////////////////////////////////////////////////////////
// CONSTANTS
///////////////////////////////////////////////////////////////////////
byte mac[] = {  0x90, 0xA2, 0xDA, 0x00, 0x00, 0x00 };
byte client_server[] = { 192, 168, 1, 99 };   //Manual setup only
int client_port = 8080;
//byte subnet[] = { 255, 255, 255, 0 }; //Manual setup only
String game = "beerpong";
String parameter1 = "reference=" + game;
String parameter2 = "&serial=";
String parameter = parameter1 + parameter2;
      
unsigned long time;
unsigned long responseTime;

// LEDS
int ledcolor = 0;
int red = 4; //this sets the red led pin
int green = 5; //this sets the green led pin
int blue = 6; //this sets the blue led pin

void setup()
{
  // Pinmodes
  pinMode (ETH_SS, OUTPUT); 
  pinMode (NFC_SS, OUTPUT); 
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  
  // Digital Write
  digitalWrite(ETH_SS, HIGH);

  time = millis();
  Serial.begin(9600);
  Serial.println("Starting setup method...");

  //Initialise NFC reader
  nfc.backupSPIConf();
  nfc.begin();
  nfc.RFConfiguration(0x14); // default is 0xFF (try forever; ultimately it does time out but after a long while
                             // modifies NFC library to set up a timeout while searching for RFID tags
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    // stop
    for(;;);
  }    
  // ok, print received data!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  Serial.print("Supports "); Serial.println(versiondata & 0xFF, HEX);
  // configure board to read RFID tags and cards
  nfc.SAMConfig();
  nfc.restoreSPIConf();
 
 
  //Initialise Ethernet connection
  Serial.println("StartEthernet");
  digitalWrite(10, LOW); //SPI select Ethernet
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // stop
    for(;;);
  }
  digitalWrite(10, HIGH); //SPI deselect Ethernet

  Serial.println("NFC and Ethernet initialised OK");   
  flowState=STATE_IDDLE;
  delay(1000);
}

void loop(){ 
  if ((millis()-time > 1000)&&(flowState==STATE_IDDLE)) {
      Serial.println("Checking NFC...");
    // look for Mifare type cards every second
    time=millis();
    digitalWrite(10, HIGH);//SPI deselect Ethernet    
    nfc.backupSPIConf();
    Serial.println("Start NFC read");
    tagId = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A);
    Serial.println("End NFC read");
    nfc.restoreSPIConf();
    if (tagId != 0) 
    { 
      Serial.print("Read card #"); Serial.println(tagId);
      xmitId=0;
      uint32_t divisor= 1000000000;
      for (int i=0;i<10; i++){
         tagIdString [i]= char(48 + tagId/divisor);
         tagId=tagId%divisor;
         divisor /=10;
      }
      Serial.print("Converted String: "); 
      Serial.println(tagIdString);
      time=millis();
      flowState=STATE_SENDDATA;
      return;
     }
  }   

  if (flowState==STATE_SENDDATA) {
    if (client.connect(client_server, client_port)) {      
      Serial.println(F("Making HTTP Call"));
      client.println(F("POST /app_dev.php/api/v1/scoreboards.json HTTP/1.1"));
      client.println(F("Host: 192.168.1.99"));
      client.println(F("Content-Type: application/x-www-form-urlencoded"));
      client.print(F("Content-Length: "));
      
      String total_parameter = parameter + tagIdString;
      const char *str = total_parameter.c_str();
    
      client.println(strlen(str));
      client.println(F("Connection: close\r\n"));
      client.println(str);

      responseTime=millis();
      flowState=STATE_RESPONSE;
    } else {
      Serial.println("Connection to server failed");       
      flowState=STATE_IDDLE;
    }
  }
  
  if (flowState== STATE_RESPONSE) {
    if (client.available()) {
       char c = client.read();  
//                       Serial.print(c);
          if(c == '{'){
            reading = true;
          }
          if(reading && proceed){
            if(c == 'g' || gTrue){
                x++;
                gTrue = true;
                if(x <= 4 ||  valueTrue){
                  valueTrue = true;
                  if(x > 5){
                    if(c == '\"'){
                      valueTrue = false;
                      gTrue = false;
                    }else{
                      strResponse += c;
                    }
                  }
                }
              }

            if(c == '}'){
              proceed = false;            
            }
          }
       }

    if ((millis() - responseTime)>1000) {
        Serial.println("Closing connection to server");
        client.stop();
        
        resultBracelet(strResponse);
        
        flowState=STATE_IDDLE;
    }
  }  
}  

void resultBracelet(String result){
  Serial.println(result);
  resetValues();
  if(result == "success"){
      digitalWrite(green, HIGH);
      delay(1000);
      digitalWrite(green, LOW);
  }else if(result == "error"){
      digitalWrite(green, HIGH);
      digitalWrite(red, HIGH);
  }else if(result == "not_exist"){
      digitalWrite(red, HIGH);
  }else{
      digitalWrite(red, HIGH);
      delay(200);
      digitalWrite(red, LOW);
      delay(200);
      digitalWrite(red, HIGH);
      delay(200);
      digitalWrite(red, LOW);   
      delay(200);
      digitalWrite(red, HIGH);
      delay(200);
  }
    switchLed();
}

void resetValues(){
  reading = false;
  proceed = true;
  gTrue = false;
  valueTrue = false;
  strResponse = "";
  x = 0;
}

void switchLed(){
  delay(1000);
  digitalWrite(green, LOW);
  digitalWrite(red, LOW);
  analogWrite(blue, LOW);
  
//  digitalWrite(green, HIGH);
//  digitalWrite(red, HIGH);
//  digitalWrite(blue, HIGH);
}
